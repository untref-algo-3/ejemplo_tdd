#include <stdlib.h>
#include "main_test.hpp"
#include "../src/capicua.h"

int *numero;

void alocarNumero(int cantidadDigitos) {
  numero = (int *)malloc(sizeof(int) * cantidadDigitos);
}

void liberarNumero() {
  if (numero != NULL) {
    free(numero);
  }

  numero = NULL;
}

TEST_CASE("numero con un unico digito deberia ser capicua") {
  alocarNumero(1);

  numero[0] = 3;

  REQUIRE(esCapicua(numero, 1) == true);

  liberarNumero();
}

TEST_CASE("numero con dos digitos distintos no deberia ser capicua") {
  alocarNumero(2);

  numero[0] = 3;
  numero[1] = 9;

  REQUIRE(esCapicua(numero, 2) == false);

  liberarNumero();
}

TEST_CASE("numero con dos digitos iguales deberia ser capicua") {
  alocarNumero(2);

  numero[0] = 9;
  numero[1] = 9;

  REQUIRE(esCapicua(numero, 2) == true);

  liberarNumero();
}

TEST_CASE("deberia devolver true para numero capicua de tres digitos") {
  alocarNumero(3);

  numero[0] = 9;
  numero[1] = 1;
  numero[2] = 9;

  REQUIRE(esCapicua(numero, 3) == true);

  liberarNumero();
}

TEST_CASE("deberia devolver false para numero no capicua de tres digitos") {
  alocarNumero(3);

  numero[0] = 9;
  numero[1] = 1;
  numero[2] = 1;

  REQUIRE(esCapicua(numero, 3) == false);

  liberarNumero();
}

TEST_CASE("deberia devolver true para numero capicua con cantidad de digitos pares") {
  alocarNumero(8);

  numero[0] = 9;
  numero[1] = 8;
  numero[2] = 7;
  numero[3] = 6;
  numero[4] = 6;
  numero[5] = 7;
  numero[6] = 8;
  numero[7] = 9;

  REQUIRE(esCapicua(numero, 8) == true);

  liberarNumero();
}

TEST_CASE("deberia devolver false para numero no capicua con cantidad de digitos pares") {
  alocarNumero(8);

  numero[0] = 9;
  numero[1] = 8;
  numero[2] = 7;
  numero[3] = 6;
  numero[4] = 5;
  numero[5] = 7;
  numero[6] = 8;
  numero[7] = 9;

  REQUIRE(esCapicua(numero, 8) == false);

  liberarNumero();
}

TEST_CASE("deberia devolver false si puntero a numero es NULL") {
  REQUIRE(esCapicua(NULL, 8) == false);
}

TEST_CASE("deberia devolver false si cantidad de digitos es 0") {
  alocarNumero(1);

  numero[0] = 3;

  REQUIRE(esCapicua(numero, 0) == false);

  liberarNumero();
}
