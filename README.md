# Ejemplo de uso de TDD en C

En este ejemplo se usa [Catch2](https://github.com/catchorg/Catch2) para implementar usando TDD el ejercicio de número capicúa.
Para compilar y ejecutar en la máquina virtual:

```
cd clase_tdd/
g++ tests/capicua_tests.cpp src/capicua.c -o out/capicua_tests
out/capicua_tests
```
