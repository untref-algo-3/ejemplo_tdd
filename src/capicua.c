#include <stdlib.h>
#include "capicua.h"

bool esCapicua(int *numero, int cantidadDigitos) {
  if (numero == NULL || cantidadDigitos == 0) {
    return false;
  }

  int indiceIzquierdo = 0,
      indiceDerecho = cantidadDigitos - 1;

  while (indiceIzquierdo < indiceDerecho) {
    if (numero[indiceIzquierdo] != numero[indiceDerecho]) {
      return false;
    }

    indiceIzquierdo++;
    indiceDerecho--;
  }

  return true;
}
